import "./import/blocks.js";

$('.content-area').masonry({
	// options...
	itemSelector: '.product-item',
	columnWidth: '.grid-sizer',
	percentPosition: true
});